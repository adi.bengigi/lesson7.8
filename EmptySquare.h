#pragma once
#include "tools.h"

// This class is the empty squares in the board
class Empty : public tools
{
public:
	//ctor
	Empty(int color);
	//dtor
	~Empty();
	// moving function
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&board)[SIZE_BOARD][SIZE_BOARD]);
};