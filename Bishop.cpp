#include "Bishop.h"
#include "board.h"

Bishop::Bishop()
{
}

Bishop::Bishop(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'b';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'B';
	}
}

Bishop::~Bishop()
{
}

string Bishop::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// liggle movement for tool:
	// bishop

	int i = source[0], j = source[1];

	// valid move up left corner
	if ((source[0] - dest[0] == source[1] - dest[1]) && source[0] - dest[0] > 0) {
		for (i -= 1, j -= 1; i > dest[0]; i--, j--)
			if (_board[i][j]->getColor() != EMPTY)
				return RESULT_FIVE;
		return RESULT_ZERO;
	}
	// valid move up right corner
	if ((source[0] - dest[0] == dest[1] - source[1]) && source[0] - dest[0] > 0) {
		for (i -= 1, j += 1; i > dest[0]; i--, j++)
			if (_board[i][j]->getColor() != EMPTY)
				return RESULT_FIVE;
		return RESULT_ZERO;
	}
	// valid move down left corner
	if ((dest[0] - source[0] == source[1] - dest[1]) && dest[0] - source[0] > 0) {
		for (i += 1, j -= 1; i < dest[0]; i++, j--)
			if (_board[i][j]->getColor() != EMPTY)
				return RESULT_FIVE;
		return RESULT_ZERO;
	}
	// valid move down right corner
	if ((dest[0] - source[0] == dest[1] - source[1]) && dest[0] - source[0] > 0) {
		for (i += 1, j += 1; i < dest[0]; i++, j++)
			if (_board[i][j]->getColor() != EMPTY)
				return RESULT_FIVE;
		return RESULT_ZERO;
	}

	return RESULT_SIX;
}
