#include "Pawn.h"
#include "board.h"

Pawn::Pawn(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'p';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'P';
	}
}

Pawn::~Pawn()
{
}