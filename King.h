#pragma once
#include "tools.h"
#include "EmptySquare.h"

// This class is the empty squares in the board
class King : public tools
{
public:
	//ctor
	King(int color);
	//dtor
	~King();
	// moving function
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]);

};